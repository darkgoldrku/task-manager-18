package ru.t1.bugakov.tm.command.project;

import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectChangeStatusByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getProjectService().changeProjectStatusByIndex(index, status);
    }

    @Override
    public String getName() {
        return "project-change-status-by-index";
    }

    @Override
    public String getDescription() {
        return "Change project status by index.";
    }

}
