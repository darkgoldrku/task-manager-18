package ru.t1.bugakov.tm.command.task;

import ru.t1.bugakov.tm.model.Task;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = getTaskService().findOneByIndex(index);
        showTask(task);
    }

    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public String getDescription() {
        return "Show task by index.";
    }

}
