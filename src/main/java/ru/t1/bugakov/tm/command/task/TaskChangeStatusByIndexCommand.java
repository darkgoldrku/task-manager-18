package ru.t1.bugakov.tm.command.task;

import ru.t1.bugakov.tm.enumerated.Status;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(index, status);
    }

    @Override
    public String getName() {
        return "task-change-status-by-index";
    }

    @Override
    public String getDescription() {
        return "Change task status by index.";
    }

}
