package ru.t1.bugakov.tm.command.task;

import ru.t1.bugakov.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(id);
    }

    @Override
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    public String getDescription() {
        return "Remove task by id.";
    }

}
