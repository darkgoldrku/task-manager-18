package ru.t1.bugakov.tm.command.project;

import ru.t1.bugakov.tm.model.Project;
import ru.t1.bugakov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        getProjectTaskService().removeProjectById(project.getId());
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public String getDescription() {
        return "Remove project by index.";
    }

}
